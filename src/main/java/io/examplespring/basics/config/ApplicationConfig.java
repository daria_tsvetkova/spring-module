package io.examplespring.basics.config;

import io.examplespring.basics.entity.Bus;
import io.examplespring.basics.entity.Passenger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

@Configuration
@ComponentScan(basePackageClasses = Bus.class)
public class ApplicationConfig {

    @Bean
    @Scope(scopeName = "prototype")
    public Passenger prototypeBean() {
        List<String> bob = List.of("Bob", "Mary", "Leon", "Max", "Elena", "Irina");
        String name = bob.get(ThreadLocalRandom.current().nextInt(bob.size()));
        return new Passenger(name);
    }
}